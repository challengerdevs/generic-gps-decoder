'use strict';

module.exports = (packets, name) => {
  return packets.filter(packetSchema => name === packetSchema.name)
    .reduce((prev, next) => {
      return next;
    }, undefined);
}