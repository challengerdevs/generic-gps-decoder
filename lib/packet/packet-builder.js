'use strict';

const Packet = require('./packet');
const findPacketSchema = require('../utils/find-packet-schema');

function getFixedValue(fieldSchema, parsedValue) {
  if (!fieldSchema.fixedValues) {
    return parsedValue;
  }

  return fieldSchema.fixedValues.filter(fixedValue => parsedValue === fixedValue.key)
    .reduce((prev, next) => {
      return next.value;
    }, undefined);
}

function operate(operator, leftOperand, rightOperand) {
  switch (operator) {
    case 'div':
      return leftOperand / rightOperand;
    case 'mult':
      return leftOperand * rightOperand;
    case 'sum':
      return leftOperand + rightOperand;
    case 'diff':
      return leftOperand - rightOperand;
    case 'mod':
      return leftOperand % rightOperand;
  }
}

function reduceOperations(collection, initialValue) {
  return collection.reduce((previousValue, current) => {
    return current.params.reduce((prev, next) => {
      return operate(current.operation, prev, next);
    }, previousValue);
  }, initialValue);
}

function operateAfterDecode({ additionalParseOperations }, initialValue) {
  if (!additionalParseOperations) {
    return initialValue;
  }
  return reduceOperations(additionalParseOperations, initialValue);
}


function isConditionAccepted(condition, left, right) {
  switch(condition) {
    case 'gt':
      return left > right;
    case 'gte':
      return left >= right;
    case 'lt':
      return left < right;
    case 'lte':
      return left <= right;
    case 'eq':
      return left === right;
    case 'neq':
      return left !== right;
  }
}

function reduceConditional(collection, initialValue) {
  return collection.reduce((previousValue, current) => {
    if (!isConditionAccepted(current.condition, previousValue, current.value)) {
      return previousValue;
    }
    return reduceOperations(current.operations, previousValue);
  }, initialValue);
}

function operateConditional({ conditionalOperations }, initialValue) {
  if (!conditionalOperations) {
    return initialValue;
  }
  return reduceConditional(conditionalOperations, initialValue);
}

const createTraceDecoder = (decodeFn) => (fieldSchema, trace) => {
  let decodedValue = decodeFn(fieldSchema, trace);
  decodedValue = operateConditional(fieldSchema, decodedValue);
  return getFixedValue(fieldSchema, operateAfterDecode(fieldSchema, decodedValue));
};

module.exports = ({ packetType }, trace, decodeFn) => {
  const content = {};
  let expectResponse = false;
  const getValueFromTrace = createTraceDecoder(decodeFn);
  const type = getValueFromTrace(packetType, trace);
  const builder = {
    addContent({ packets }) {
      const packetSchema = findPacketSchema(packets, type); 
      if (!packetSchema) {
        throw new Error(`Undefined packet definition: ${type}`);
      }
      packetSchema.records.forEach((record) => {
        const decodedValue = getValueFromTrace(record, trace);
        content[record.field] = decodedValue;
      });
      expectResponse = packetSchema.expectResponse;
      return this;
    },
    build() {
      const packet = new Packet(type, expectResponse);
      packet.content = Object.assign(packet.content, content);
      return Object.freeze(packet);
    },
  };
  return builder;
}