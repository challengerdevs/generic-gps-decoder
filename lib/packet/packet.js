'use strict';

module.exports = class Packet {
  constructor(type, expectResponse) {
    this.packetType = type;
    this.expectResponse = expectResponse;
    this.content = {};
  }
};