'use strict';

const Joi = require('joi');

const operation = Joi.object().keys({
  operation: Joi.string().regex(/^div|mult|sum|diff|mod$/, 'operation').required(),
  params: Joi.array().items(Joi.string(), Joi.number()).required(),
});

const condition = Joi.object().keys({
  condition: Joi.string().regex(/^gt|gte|lt|lte|eq|neq$/, 'condition').required(),
  value: Joi.any().required(),
  operations: Joi.array().items(operation).required(),
});

const additionalParseOperations = Joi.array().items(operation);

const fixedValues = Joi.array().items(Joi.object().keys({
  key: Joi.number().integer().required(),
  value: Joi.any().required(),
})).min(1);

const byteContent = Joi.object().keys({
  field: Joi.string().required(),
  index: Joi.number().integer().required(),
  length: Joi.number().integer().required(),
  parsedType: Joi.string().default('string'),
  conditionalOperations: Joi.array().items(condition),
  additionalParseOperations,
  fixedValues,
});

const responseContent = Joi.object().keys({
  field: Joi.string().required(),
  index: Joi.number().integer().required(),
  length: Joi.number().integer().required(),
  value: Joi.number().integer(),
  valueFromPacket: Joi.string(),
}).xor('value', 'valueFromPacket');

const response = Joi.object().keys({
  name: Joi.string().required(),
  records: Joi.array().items(responseContent).required(),
});

const packets = Joi.array().items(Joi.object().keys({
  name: Joi.string().required(),
  expectResponse: Joi.boolean().default(false),
  records: Joi.array().items(byteContent).required(),
}));

const protocol = Joi.object().keys({
  encoding: Joi.string().default('hex'),
  packetType: byteContent.required(),
  packets,
  responses: Joi.array().items(response),
});

module.exports = Joi.object().keys({
  name: Joi.string().required(),
  protocol,
});