'use strict';

const hexParser = require('./hex-decoder');

module.exports = (protocol) => {
  const encoding = protocol.encoding;
  switch (encoding) {
    case 'hex':
      return hexParser(protocol);
    default:
      throw new Error(`Encoding type "${encoding}" not supported`);
  };
};