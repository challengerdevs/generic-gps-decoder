'use strict';

const packetBuilderFactory = require('../packet/packet-builder');

function parseToHexString({ index, length }, trace) {
  let parsedString = '';
  const top = index + length;
  for (let i = index; i < top; i++) {
    parsedString += trace[i].toString(16);
  }
  return parsedString;
}

function parseToString({ index, length }, trace) {
  let parsedString = '';
  for (let i = index; i < index + length; i++) {
    parsedString += trace[i];
  }
  return parsedString;
}

function parseToNumber({ index, length }, trace) {
  let accum = 0;
  let power = 0;
  for (let i = length + index - 1; i >= index; i--) {
    accum += trace[i] * Math.pow(256, power);
    power++;
  }
  return accum;
}

function parseByType(fieldSchema, trace) {
  switch (fieldSchema.parsedType) {
    case 'string':
      return parseToString(fieldSchema, trace);
    case 'hex':
      return parseToHexString(fieldSchema, trace);
    case 'number':
      return parseToNumber(fieldSchema, trace);
    default:
      throw new Error(`Unsuported parsedType: ${fieldSchema.parsedType}`);
  }
}

/**
 * Factory module for hex decoder
 * @param {Object} protocol, the protocol to use for parse
 * @return {parser}, a parser object
 */
module.exports = (protocol) => {
  const decoder = {
    /**
     * @function decode decodes the given binary trace
     * @param {Buffer} trace, the buffer to be parsed within the protocol
     * @return {Packet}, an object with the parsed fields 
     */
    decode(trace) {
      const packet = packetBuilderFactory(protocol, trace, parseByType)
        .addContent(protocol)
        .build();
      return packet;
    },
  };
  return decoder;
};