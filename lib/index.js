'use strict';

const Promise = require('bluebird');
const protocolSchema = require('./protocol-schema.js');
const loadDecoder = require('./decoders');
const loadEncoder = require('./encoders');

const validateProtocol = Promise.promisify(protocolSchema.validate, { context: protocolSchema });


/** 
 * decoder factory module
 * @param {Object} protocolConfiguration, object that contains the protocol configuration values based
 * on the protocol configuration schema
 * @return {Promise}, a promise that will be resolved with the configured decoder.  
 */
module.exports = protocolConfiguration => validateProtocol(protocolConfiguration)
  .then(config => {
    const innerDecoder = loadDecoder(config.protocol);
    const encoder = loadEncoder(config.protocol);
    
    /**
     * @var {decoder}, a Decoder that will encode and decode traces with the given protocol config
     */
    const gpsDecoder = {
      /**
       * @function decodeTrace
       * @param {Buffer} trace, the buffer to get decoded with the given protocol
       * @return {Packet}, an object that will contain the content of the given trace
       */
      decodeTrace(trace) {
        const packet = innerDecoder.decode(trace);
        return packet;
      },

      /**
       * @function encodeResponsePacket
       * @param {Packet} packet, the packet from which will get the encoded response
       */
      encodeResponsePacket(packet) {
        const encodedResponse = encoder.encode(packet);
        return encodedResponse;
      },
    };
    return gpsDecoder;
  });