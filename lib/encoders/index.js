'use strict';

const hexEncoder = require('./hex-encoder');

module.exports = (protocol) => {
  const encoding = protocol.encoding;
  switch(encoding) {
    case 'hex':
      return hexEncoder(protocol);
    default:
      throw new Error(`Encoding type "${encoding}" not supported`);
  }
};