'use strict';

const findPacketSchema = require('../utils/find-packet-schema');

function getValueFromPacket(from, packet) {
  if (!packet.content[from]) {
    throw new Error(`Undefined content field on package: ${from}`);
  }
  return packet.content[from];  
}

function getHexArray(value, length) {
  const hexArray = [];
  let accum = value;
  for (let i = length - 1; i >= 0 ; i--) {
    const val = Math.floor(accum / Math.pow(256, i));
    hexArray.push(val);  
  }
  return hexArray;
}

function getResponseFromSchema(responsesSchema, packet) {
  return responsesSchema.records.reduce((accum, current) => {
    if (current.valueFromPacket) {
      const value = getValueFromPacket(current.valueFromPacket, packet);
      return accum.concat(getHexArray(value, current.length));
    }
    return accum.concat(getHexArray(current.value, current.length));
  }, []);
}

/**
 * Factory module for hex encoders
 * @param {Object} protocol, the protocol to use for encoding
 * @return {encoder}, an encoder object
 */
module.exports = (protocol) => {
  const encoder = {
    /**
     * @function encode encode the response within the given packet
     * @param {Packet} packet, the packet from which the response is encoded
     * @return {Buffer} a Hex encoded string based buffer
     */
    encode(packet) {
      const packetType = packet.packetType;
      const packetSchema = findPacketSchema(protocol.responses, packetType);
      if (!packetSchema) {
        throw new Error(`Undefined packet type: ${packetType}`);
      }
      const bytes = getResponseFromSchema(packetSchema, packet);
      return Buffer.from(bytes);
    },
  };
  return encoder;
};