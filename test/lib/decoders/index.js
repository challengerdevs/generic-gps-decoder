'use strict';

const expect = require('chai').expect;
const loadDecoder = require('../../../lib/decoders');
const protocol = require('../../fixtures/elink-definition.json').protocol;

describe('inner decoders factory', () => {
  it('should throw an error while creating an unknow decoder', () => {
    try {
      loadDecoder({ encoding: 'myEncodingType' });
    } catch(err) {
      expect(err).to.not.be.null;
      expect(err.message).to.equal('Encoding type "myEncodingType" not supported');
    }
  });

  it('should create a hex decoder', () => {
    const hexDecoder = loadDecoder(protocol);
    expect(hexDecoder).to.not.be.null;
    expect(hexDecoder).to.not.be.undefined;
    expect(hexDecoder).to.include.keys('decode');
  });
});