'use strict';

const expect = require('chai').expect;
const hexDecoderFactory = require('../../../lib/decoders/hex-decoder');

const loginTrace = require('../../fixtures/binary-packets').loginTrace;
const gpsTrace = require('../../fixtures/binary-packets').gpsTrace;
const smsTrace = require('../../fixtures/binary-packets').smsTrace;
const unknownTrace = require('../../fixtures/binary-packets').unknownTrace;
const badProtocolDefinition = require('../../fixtures/bad-protocol.json');
const protocol = require('../../fixtures/elink-definition.json').protocol;

const expectedGPSPacket = {
  packetType: 'gpsData',
  expectResponse: false,
  content: { 
    header: 26471,
    length: 12,
    serialNumber: 1,
    dateTime: 252645135,
    latitude: 22.546096666666667,
    longitude: -90.43192555555555,
    speed: 160,
    course: 255,
    mcc: 460,
    mcn: 1,
    lac: 10057,
    ci: 3310,
    gpsPositioned: false,
    batteryVoltage: 100.15,
  },
};

const expectedSMSPacket = {
  packetType: 'smsCommandsUpload',
  expectResponse: false,
  content: { 
    header: 26471,
    length: 12,
    serialNumber: 1,
    dateTime: 252645135,
    latitude: 22.546096666666667,
    longitude: -90.43192555555555,
    speed: 160,
    course: 255,
    mcc: 460,
    mcn: 1,
    lac: 10057,
    ci: 3310,
    gpsPositioned: false,
    phoneNumber: '000000000000000000000',
  },
};


describe('hex-decoder', () => {
  it('should fail decoding a trace due to a parse type', () => {
    const hexDecoder = hexDecoderFactory(badProtocolDefinition.protocol);
    try {
      hexDecoder.decode(loginTrace);
    } catch(err) {
      expect(err).to.not.be.null;
      expect(err.message).to.equal('Unsuported parsedType: decimal');
    }
  });

  it('should fail decoding a trace due to an unexistent packetType', () => {
    const hexDecoder = hexDecoderFactory(protocol);
    try {
      hexDecoder.decode(unknownTrace);
    } catch(err) {
      expect(err).to.not.be.null;
      expect(err.message).to.equal('Undefined packet definition: undefined');
    }
  });

  it('should decode a GPS data packet', () => {
    const hexDecoder = hexDecoderFactory(protocol);
    const packet = hexDecoder.decode(gpsTrace);
    expect(packet).to.deep.equal(expectedGPSPacket);
  });

  it('should decode a GPS data packet with phone number', () => {
    const hexDecoder = hexDecoderFactory(protocol);
    const packet = hexDecoder.decode(smsTrace);
    expect(packet).to.deep.equal(expectedSMSPacket);
  });
});