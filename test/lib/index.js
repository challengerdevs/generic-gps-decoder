'use strict';

const expect = require('chai').expect;
const gpsDecoderFactory = require('../../lib');
const protocolDefinition = require('../fixtures/elink-definition.json');

const loginTrace = require('../fixtures/binary-packets').loginTrace;

const expectedLoginPacket = {
  packetType: 'login',
  expectResponse: true,
  content:{ 
    header: 26471,
    length: 12,
    serialNumber: 1,
    terminalId: '12345678912345',
    language: 'en_US',
    timezone: 8,
  },
};

describe('generic-gps-decoder module', () => {
  it('should fail to load an empty configuration', (done) => {
    gpsDecoderFactory({})
      .catch((err) => {
        expect(err).to.be.not.null;
        return done();
      });
  });

  it('should load a decoder with a given definition', (done) => {
    gpsDecoderFactory(protocolDefinition)
      .then((decoder) => {
        expect(decoder).to.be.not.null;
        return done();
      })
      .catch(done);
  });

  it('should decode a trace', (done) => {
    gpsDecoderFactory(protocolDefinition)
      .then((decoder) => {
        const trace = Buffer.from(loginTrace)
        const decodedPacket = decoder.decodeTrace(trace);
        expect(decodedPacket).to.deep.equal(expectedLoginPacket);
        return done();
      })
      .catch(done);
  });

  it('should encode the packet response', (done) => {
    gpsDecoderFactory(protocolDefinition)
      .then((decoder) => {
        const trace = Buffer.from(loginTrace)
        const decodedPacket = decoder.decodeTrace(trace);
        expect(decodedPacket).to.deep.equal(expectedLoginPacket);

        const responseBuffer = decoder.encodeResponsePacket(decodedPacket);
        expect(responseBuffer).to.deep.equal(Buffer.from([0x67, 0x67, 0x01, 0x00, 0x02, 0x00, 0x01]));

        return done();
      })
      .catch(done);
  });
});