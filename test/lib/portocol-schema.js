'use strict';

const expect = require('chai').expect;
const protocolSchema = require('../../lib/protocol-schema');
const protocolDefinition = require('../fixtures/elink-definition.json');

describe('protocolSchema definition', () => {
  it('should match the protocol schema', (done) => {
    protocolSchema.validate(protocolDefinition, (err, protocol) => {
      if (err) {
        return done(err);
      }
      expect(protocol).to.deep.equal(protocolDefinition);
      return done();
    });
  });
});