'use strict';

const expect = require('chai').expect;
const hexEncoderFactory = require('../../../lib/encoders/hex-encoder');
const protocol = require('../../fixtures/elink-definition').protocol;

describe('hex-encoder', () => {
  it('should fail encoding a trace due to an inexistent response definition', () => {
    const hexEncoder = hexEncoderFactory(protocol);
    try {
      const toEncode = {
        packetType: 'failure',
        expectResponse: false,
        content: {},
      };
      hexEncoder.encode(toEncode);
    } catch(err) {
      expect(err).to.be.not.null;
      expect(err).to.be.not.undefined;
      expect(err.message).to.equal('Undefined packet type: failure');
    }
  });

  it('should fail encoding a trace due to an inexistent content required field', () => {
    const hexEncoder = hexEncoderFactory(protocol);
    try {
      const toEncode = {
        packetType: 'login',
        expectResponse: false,
        content: {},
      };
      hexEncoder.encode(toEncode);
    } catch(err) {
      expect(err).to.be.not.null;
      expect(err).to.be.not.undefined;
      expect(err.message).to.equal('Undefined content field on package: header');
    }
  });
});