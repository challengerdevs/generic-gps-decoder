'use strict';

const expect = require('chai').expect;
const loadEncoder = require('../../../lib/encoders');
const protocol = require('../../fixtures/elink-definition.json').protocol;

describe('inner decoders factory', () => {
  it('should throw an error while creating an unknow decoder', () => {
    try {
      loadEncoder({ encoding: 'myEncodingType' });
    } catch(err) {
      expect(err).to.not.be.null;
      expect(err.message).to.equal('Encoding type "myEncodingType" not supported');
    }
  });

  it('should create a hex encoder', () => {
    const hexEncoder = loadEncoder(protocol);
    expect(hexEncoder).to.not.be.null;
    expect(hexEncoder).to.not.be.undefined;
    expect(hexEncoder).to.include.keys('encode');
  });
});